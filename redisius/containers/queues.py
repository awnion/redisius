import redisius


class Registry(object):
    _queues_registry = dict()

    def add(self, queue):
        if queue.fullname in self._queues_registry:
            raise Exception('Queue {} already in use'.format(queue.fullname))
        self._queues_registry[queue.fullname] = queue

    def get_stat(self):
        return [(fullname, queue.len())
                for fullname, queue in self._queues_registry.iteritems()]


registry = Registry()


class MetaQueue(type):
    def __init__(cls, name, bases, dct):
        super(MetaQueue, cls).__init__(name, bases, dct)

        if dct['__module__'] == 'redisius.containers.queues':
            return

        cls.fullname = dct.get('fullname')

        if cls.fullname is None:
            cls.name = dct.get('name') or name
            cls.fullname = ':'.join([cls.global_namespace,
                                     cls.namespace, cls.name])

        if cls.fullname is None:
            raise Exception('Fullname of queue is empty for some reason')

        registry.add(cls)

    def __getitem__(cls, item):
        connection = redisius.get_connection()
        return connection.lindex(cls.fullname, ~item)

    def __iter__(cls):
        connection = redisius.get_connection()
        for x in connection.lrange(cls.fullname, 0, -1)[::-1]:
            yield x


class Queue(object):
    """
    Use can setup `global_namespace`, `namespace` and `name` or
    set `fullname` manually
    """

    __metaclass__ = MetaQueue

    global_namespace = 'queues'
    namespace = 'main'
    name = None
    # or
    fullname = None

    batch_size = 1000
    connection = None

    @classmethod
    def len(cls, connection=None):
        connection = connection or redisius.get_connection()
        return connection.llen(cls.fullname)

    @classmethod
    def drop(cls, connection=None):
        connection = connection or redisius.get_connection()
        return connection.delete(cls.fullname)

    @classmethod
    def purge(cls, connection=None):
        connection = connection or redisius.get_connection()
        return connection.delete(cls.fullname)

    @classmethod
    def pop(cls, connection=None):
        connection = connection or redisius.get_connection()
        return connection.rpop(cls.fullname)

    @classmethod
    def push(cls, *values, **kwargs):
        connection = kwargs.get('connection') or redisius.get_connection()
        return connection.lpush(cls.fullname, *values)

    @classmethod
    def poppush(cls, queue, connection=None):
        connection = connection or redisius.get_connection()
        return connection.rpoplpush(cls.fullname, queue.fullname)
