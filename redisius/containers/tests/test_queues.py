import unittest

from redisius import containers


class QueueMetaTestCase(unittest.TestCase):
    def test_auto_name(self):
        class TQ1(containers.Queue):
            pass

        self.assertEqual(TQ1.fullname, 'queues:main:TQ1')

    def test_fullname(self):
        class TQ2(containers.Queue):
            fullname = 'foo:bar:1'

        self.assertEqual(TQ2.fullname, 'foo:bar:1')

    def test_name(self):
        class TQ3(containers.Queue):
            name = 'foo'

        self.assertEqual(TQ3.fullname, 'queues:main:foo')

    def test_namespace(self):
        class TQ4(containers.Queue):
            namespace = 'bar'
            name = 'foo'

        self.assertEqual(TQ4.fullname, 'queues:bar:foo')

    def test_global_namespace(self):
        class TQ5(containers.Queue):
            global_namespace = 'baz'
            namespace = 'bar'
            name = 'foo'

        self.assertEqual(TQ5.fullname, 'baz:bar:foo')


class SimpleQueue(containers.Queue):
    name = 'simple'


class SimpleQueue2(containers.Queue):
    name = 'simple2'


class QueueTestCase(unittest.TestCase):
    def setUp(self):
        SimpleQueue.drop()

    def test_drop_empty(self):
        SimpleQueue.drop()
        self.assertEqual(SimpleQueue.len(), 0)

    def test_purge_empty(self):
        SimpleQueue.purge()
        self.assertEqual(SimpleQueue.len(), 0)

    def test_push(self):
        SimpleQueue.push(1)
        SimpleQueue.push(2, 2, 2, 3)
        self.assertEqual(SimpleQueue.len(), 5)

    def test_pop(self):
        SimpleQueue.push('1')
        self.assertEqual(SimpleQueue.pop(), '1')

    def test_iteration(self):
        items = ['1', '2', '3', '4', '5']
        SimpleQueue.push(*items)
        self.assertListEqual([x for x in SimpleQueue], items)

    def test_pop_push(self):
        SimpleQueue.push('1')
        SimpleQueue.push('2')
        self.assertEqual(SimpleQueue.poppush(SimpleQueue2), '1')
        self.assertEqual(SimpleQueue2.pop(), '1')
