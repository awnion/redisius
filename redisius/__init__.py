import redis


__all__ = ['settings', 'get_connection']


def connect():
    global pool
    global connection
    if settings.USE_POOL:
        pool = redis.ConnectionPool(
            host=settings.HOST,
            port=settings.PORT,
            db=settings.DB,
            max_connections=settings.MAX_CONNECTIONS,
            password=settings.PASSWORD,
        )
    else:
        connection = redis.StrictRedis(
            host=settings.HOST,
            port=settings.PORT,
            db=settings.DB,
            password=settings.PASSWORD,
        )


def get_connection():
    if not pool and not connection:
        connect()
    if pool:
        return redis.StrictRedis(connection_pool=pool)
    return connection


class Settings(object):
    HOST = 'localhost'
    PORT = 6379
    DB = 0
    PASSWORD = None

    # pool
    USE_POOL = True
    MAX_CONNECTIONS = None


settings = Settings()
pool = None
connection = None
