#!/usr/bin/env python
from setuptools import setup, find_packages


setup(
    name="redisius",
    version="0.0.1",
    packages=find_packages(exclude=["tests"]),
    zip_safe=False,
    author="Sergey Blinov",
    author_email="blinovsv@gmail.com",
    install_requires=[
        "redis",
    ],
    url="",
    description="",
    long_description="",
)
